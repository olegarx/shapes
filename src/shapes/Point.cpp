#include "Point.h"

Point::Point() {
	x = 0.0;
	y = 0.0;
}
Point::Point(double x, double y) {
	this->x = x;
	this->y = y;
}
Point::Point(const Point &point) {
	x = point.x;
	y = point.y;
}

void Point::setX(double x) {
	this->x = x;
}
double Point::getX() const {
	return x;
}

void Point::setY(double y) {
	this->y = y;
}
double Point::getY() const {
	return y;
}

void Point::shift(double deltaX, double deltaY) {
	x += deltaX;
	y += deltaY;
}
void Point::shiftLeft(double delta) {
	x -= delta;
}
void Point::shiftRight(double delta) {
	x += delta;
}
void Point::shiftUp(double delta) {
	y += delta;
}
void Point::shiftDown(double delta) {
	y -= delta;
}