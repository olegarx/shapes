#include "Triangle.h"
#include <sstream>

Triangle::Triangle() {
}
Triangle::Triangle(double firstPointX, double firstPointY,
	double secondPointX, double secondPointY,
	double thirdPointX, double thirdPointY) {
	
	setFirstPoint(firstPointX, firstPointY);
	setSecondPoint(secondPointX, secondPointY);
	setThirdPoint(thirdPointX, thirdPointY);
}
Triangle::Triangle(const Point &firstPoint, const Point &secondPoint, const Point &thirdPoint) {
	setFirstPoint(firstPoint);
	setSecondPoint(secondPoint);
	setThirdPoint(thirdPoint);
}
Triangle::Triangle(const Triangle &triangle) {
	firstPoint = triangle.firstPoint;
	secondPoint = triangle.secondPoint;
	thirdPoint = triangle.thirdPoint;
}

void Triangle::setFirstPoint(double x, double y) {
	this->firstPoint.setX(x);
	this->firstPoint.setY(y);
}
void Triangle::setFirstPoint(const Point &firstPoint) {
	this->firstPoint = firstPoint;
}

void Triangle::setSecondPoint(double x, double y) {
	this->secondPoint.setX(x);
	this->secondPoint.setY(y);
}
void Triangle::setSecondPoint(const Point &secondPoint) {
	this->secondPoint = secondPoint;
}

void Triangle::setThirdPoint(double x, double y) {
	this->thirdPoint.setX(x);
	this->thirdPoint.setY(y);
}
void Triangle::setThirdPoint(const Point &thirdPoint) {
	this->thirdPoint = thirdPoint;
}

Point Triangle::getFirstPoint() const {
	return firstPoint;
}
Point Triangle::getSecondPoint() const {
	return secondPoint;
}
Point Triangle::getThirdPoint() const {
	return thirdPoint;
}

double Triangle::getFirstLength() const {
	return std::sqrt(std::pow(firstPoint.getX() - secondPoint.getX(), 2) + std::pow(firstPoint.getY() - secondPoint.getY(), 2));
}
double Triangle::getSecondLength() const {
	return std::sqrt(std::pow(secondPoint.getX() - thirdPoint.getX(), 2) + std::pow(secondPoint.getY() - thirdPoint.getY(), 2));
}
double Triangle::getThirdLength() const {
	return std::sqrt(std::pow(thirdPoint.getX() - firstPoint.getX(), 2) + std::pow(thirdPoint.getY() - firstPoint.getY(), 2));
}

double Triangle::getSquare() const {
	double firstLength = getFirstLength();
	double secondLength = getSecondLength();
	double thirdLength = getThirdLength();

	double halfPerimeter = (firstLength + secondLength + thirdLength) * 0.5;

	return std::sqrt(halfPerimeter * (halfPerimeter - firstLength) * (halfPerimeter - secondLength) * (halfPerimeter - thirdLength));
}
double Triangle::getPerimeter() const {
	return getFirstLength() + getSecondLength() + getThirdLength();
}

std::string Triangle::string() const {
	std::stringstream result;
	result << "Triangle: FirstPoint(" << firstPoint.getX() << ", " << firstPoint.getY() << ") " <<
		      "SecondPoint(" << secondPoint.getX() << ", " << secondPoint.getY() << ") " << 
			  "ThirdPoint(" << thirdPoint.getX() << ", " << thirdPoint.getY() << ")";

	return result.str();
}