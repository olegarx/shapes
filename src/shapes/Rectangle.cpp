#include "Rectangle.h"
#include <sstream>

Rectangle::Rectangle() {
}
Rectangle::Rectangle(double left, double top, double right, double bottom) {
	setFirstPoint(left, top);
	setSecondPoint(right, bottom);
}
Rectangle::Rectangle(const Point &leftTop, const Point &rightBottom) {
	setFirstPoint(leftTop);
	setSecondPoint(rightBottom);
}

Rectangle::Rectangle(const Rectangle &rectangle) {
	firstPoint = rectangle.firstPoint;
	secondPoint = rectangle.secondPoint;
}

void Rectangle::setFirstPoint(double x, double y) {
	this->firstPoint.setX(x);
	this->firstPoint.setY(y);
}
void Rectangle::setFirstPoint(const Point &firstPoint) {
	this->firstPoint = firstPoint;
}

void Rectangle::setSecondPoint(double x, double y) {
	this->secondPoint.setX(x);
	this->secondPoint.setY(y);
}
void Rectangle::setSecondPoint(const Point &secondPoint) {
	this->secondPoint = secondPoint;
}

Point Rectangle::getFirstPoint() const {
	return firstPoint;
}
Point Rectangle::getSecondPoint() const {
	return secondPoint;
}

double Rectangle::getWidth() const {
	return std::abs(firstPoint.getX() - secondPoint.getX());
}
double Rectangle::getHeight() const {
	return std::abs(firstPoint.getY() - secondPoint.getY());
}

double Rectangle::getSquare() const {
	return getWidth() * getHeight();
}
double Rectangle::getPerimeter() const {
	return 2 * ( getWidth() + getHeight() );
}

std::string Rectangle::string() const {
	std::stringstream result;
	result << "Rectangle: FirstPoint(" << firstPoint.getX() << ", " << firstPoint.getY() << ") " << 
		                "SecondPoint(" << secondPoint.getX() << ", " << secondPoint.getY() << ")";

	return result.str();
}