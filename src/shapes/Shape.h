#ifndef SHAPE_H
#define SHAPE_H

#include <string>

class Shape {
public:
	virtual double getSquare() const = 0;
	virtual double getPerimeter() const = 0;

	virtual std::string string() const = 0;
private:
};

#endif