#ifndef POINT_H
#define POINT_H

class Point {
public:
	Point();
	Point(double x, double y);
	Point(const Point &point);

	void setX(double x); 
	double getX() const;

	void setY(double y);
	double getY() const;

	void shift(double deltaX, double deltaY);
	void shiftLeft(double delta);
	void shiftRight(double delta);
	void shiftUp(double delta);
	void shiftDown(double delta);
private:
	double x;
	double y;
};

#endif