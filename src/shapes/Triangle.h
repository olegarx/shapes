#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Shape.h"
#include "Point.h"

class Triangle : public Shape {
public:
	Triangle();
	Triangle(double firstPointX, double firstPointY, 
		     double secondPointX, double secondPointY,
		     double thirdPointX, double thirdPointY);
	Triangle(const Point &firstPoint, const Point &secondPoint, const Point &thirdPoint);
	Triangle(const Triangle &triangle);

	void setFirstPoint(double x, double y);
	void setFirstPoint(const Point &firstPoint);

	void setSecondPoint(double x, double y);
	void setSecondPoint(const Point &secondPoint);

	void setThirdPoint(double x, double y);
	void setThirdPoint(const Point &thirdPoint);

	Point getFirstPoint() const;
	Point getSecondPoint() const;
	Point getThirdPoint() const;

	double getFirstLength() const;
	double getSecondLength() const;
	double getThirdLength() const;

	double getSquare() const;
	double getPerimeter() const;

	std::string string() const;

private:
	Point firstPoint;
	Point secondPoint;
	Point thirdPoint;
};

#endif