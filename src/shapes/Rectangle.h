#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Shape.h"
#include "Point.h"


class Rectangle : public Shape {
public:
	Rectangle();
	Rectangle(double left, double top, double right, double bottom);
	Rectangle(const Point &leftTop, const Point &rightBottom);
	Rectangle(const Rectangle &rectangle);

	void setFirstPoint(double x, double y);
	void setFirstPoint(const Point &firstPoint);

	void setSecondPoint(double x, double y);
	void setSecondPoint(const Point &secondPoint);

	Point getFirstPoint() const;
	Point getSecondPoint() const;

	double getWidth() const;
	double getHeight() const;	

	double getSquare() const;
	double getPerimeter() const;

	std::string string() const;

private:
	Point firstPoint;
	Point secondPoint;
};

#endif