#include <iostream>
#include "shapes/Triangle.h"
#include "shapes/Rectangle.h"

void printPointInfo(const Point &point) {
	std::cout << "(x: " << point.getX() << "; y: " << point.getY() << ");" << std::endl;
}

void printShapeInfo(const Shape &shape) {
	std::cout << shape.string() << std::endl;
	std::cout << "Perimeter: " << shape.getPerimeter() << " Square: " << shape.getSquare() << std::endl;
}

int main() {
	Point firstPoint(0, 0);
	Point secondPoint(1, 0);
	Point thirdPoint(1, 1);
	Point fourthPoint(0, 1);

	Point pointCopy(secondPoint);

	std::cout << "First point: ";
	printPointInfo(firstPoint);
	std::cout << std::endl;

	std::cout << "Second point: ";
	printPointInfo(secondPoint);
	std::cout << std::endl;

	std::cout << "Copy second point: ";
	printPointInfo(pointCopy);
	std::cout << std::endl;

	std::cout << "Third point: ";
	printPointInfo(thirdPoint);
	std::cout << std::endl;

	std::cout << "Fourth point: ";
	printPointInfo(fourthPoint);
	std::cout << std::endl;


	Triangle triangleByPoints(firstPoint, secondPoint, thirdPoint);
	Rectangle rectangleByPoints(fourthPoint, secondPoint);
	
	Triangle triangle(0, 0, 1, 0, 1, 1);
	Rectangle rectangle(0, 1, 1, 0);

	Triangle triangleCopy(triangle);
	Rectangle rectangleCopy(rectangle);

	std::cout << "Triangle created by point: " << std::endl;
	printShapeInfo(triangleByPoints);	
	std::cout << std::endl;

	std::cout << "Rectangle created by point: " << std::endl;
	printShapeInfo(rectangleByPoints);
	std::cout << std::endl;

	printShapeInfo(triangle);
	std::cout << "First length: " << triangle.getFirstLength() <<
		         " Second length: " << triangle.getSecondLength() <<
		         " Third length: " << triangle.getThirdLength() << std::endl;
	std::cout << std::endl;


	printShapeInfo(rectangle);
	std::cout << "Width: " << rectangle.getWidth() << " Height: " << rectangle.getHeight() << std::endl;
	std::cout << std::endl;

	std::cout << "Triangle copy: " << std::endl;
	printShapeInfo(triangleCopy);
	std::cout << std::endl;

	std::cout << "Rectangle copy: " << std::endl;
	printShapeInfo(rectangleCopy);
	std::cout << std::endl;

	return 0;
}